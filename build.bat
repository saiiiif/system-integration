@ECHO OFF
REM 
REM Variabili di Ambiente richieste
REM 
REM   JAVA_HOME       Deve puntare alla propria Java Development Kit.
REM 
REM   MAVEN_HOME      Impostare con il path di installazione di Maven.
REM 
REM   ACTIVITI_LIB    Impostare con il path <AlfrescoInstallLocation>/tomcat/lib
REM
SETLOCAL
SET _HOME=%~dp0

SET MAVEN_HOME=C:\apache-maven-3.3.9
SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_152

SET PATH=%MAVEN_HOME%\bin;%PATH%


call %MAVEN_HOME%\bin\mvn install -Dmaven.test.skip=true

pause



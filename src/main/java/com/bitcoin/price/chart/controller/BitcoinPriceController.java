package com.bitcoin.price.chart.controller;

import com.bitcoin.price.chart.config.ApiConfig;
import com.bitcoin.price.chart.config.AppConfig;
import com.bitcoin.price.chart.responseobject.Data;
import com.bitcoin.price.chart.responseobject.PriceData;
import com.bitcoin.price.chart.responseobject.ResponseObject;
import com.bitcoin.price.chart.service.PriceDataEntityMapper;
import com.bitcoin.price.chart.store.PriceDataEntity;
import com.bitcoin.price.chart.store.PriceDataEntityRepository;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BitcoinPriceController {
    private final PriceDataEntityRepository repo;
    private final PriceDataEntityMapper mapper;
    private final ApiConfig apiConfig;
    private final AppConfig appConfig;
    private final Logger logger;

    public BitcoinPriceController(PriceDataEntityRepository repo, PriceDataEntityMapper mapper, ApiConfig apiConfig, AppConfig appConfig, Logger logger) {
        this.repo = repo;
        this.mapper = mapper;
        this.apiConfig = apiConfig;
        this.appConfig = appConfig;
        this.logger = logger;
    }

    @GetMapping("/bitcoin_charts")
    public String bitcoinCharts(Model model) {
        logger.info("Received Get Request to /bitcoin_charts");
        List<PriceDataEntity> res = repo.findByCryptoAndFiat(
                apiConfig.getCrypto(),
                apiConfig.getFiat()
        );
        Timestamp intervalStart = new Timestamp(System.currentTimeMillis() - appConfig.getIntervalMs());
        List<PriceData> priceData = res.stream()
                .filter(it -> it.getServerTimestamp().after(intervalStart))
                .map(mapper::toPriceData)
                .collect(Collectors.toList());
        Data data = mapper.toData(res.get(0));
        data.setData(priceData);
        ResponseObject obj = new ResponseObject();
        obj.setData(data);
        logger.info("Build object from db: " + obj);

        model.addAttribute("obj", obj);
        model.addAttribute("crypto_fiat", apiConfig.getCrypto() + "/" + apiConfig.getFiat());
        logger.info("Go to render bitcoin_charts");
        return "bitcoin_charts";
    }

    @GetMapping("/")
    public String index(Model model) {
        return bitcoinCharts(model);
    }

}

# System Integration 

Ein Preisticker für Kryptowährungen am Beispiel von Bitcoin und Ethereum mit Java und Spring Boot

## Getting started

#### Prerequisites

Zum Bauen:
* OpenJDK 11
* Maven

Zum Ausführen:
* eine EC2-Instanz
* eine RDS-Instanz mit einer Datenbank namens `price_data_db` und angepassten Zugangsdaten in `main/java/resources/application.properties`
* ein S3-Bucket namens `system-integration` mit den Objekten `api.json` und `config.json`
* CloudWatch mit CPU-, Speicher- und Netzwerkauslastung-Überprüfung

#### Running

Der Code lässt sich ausführen mit:
```
mvn spring-boot:run
```

## Description
In diesem Projekt haben wir Spring Boot benutzt für die Backend und für die Frontend haben wir HTML/JS.
Wir haben auch AWS (Amazon web Service ) benutzt (Ec2 , S3 , DynamoDB , Cloudwatch ).

 ### Amazon Web Service Cloud Architecture 
![ architktur Plan  ](./test69.JPG)

### Components
Das Backend kommuniziert mit RDS-Datenbank, wird über S3 konfiguriert und stellt auch das Frontend bereit. Das Deployment wurde für dieses Projekt in EC2 vorgenommen.

Mittels CloudWatch können EC2-Events wie CPU-, RAM- und Netzwerk-Auslastung überwacht werden.

### Configuration
Die Konfiguration findet über 2 JSON-Dateien im S3-Bucket `system-integration` statt. Beispielkonfiguration sind unter `./config` zu finden.

### Backend Packages

* *config*: Konfiguration mittels S3
* *controller*: Bereitstellung der Frontends aus `main/resources/templates/bitcoin_charts.html`
* *responseobject*: Daten-Klassen für [ CryptoCompare ](https://min-api.cryptocompare.com/) -API
* *service*: Abruf der CryptoCompare-API und Speicherung
* *store*: Interface zu RDS

## Usage
Wird verwendet, um die Preisänderung in der Kryptowährung zu überwachen, einfach zu nutzen und ist eine cloudbasierte Lösung.

## Contributing
Saif Jabri / Fabian Loewe 








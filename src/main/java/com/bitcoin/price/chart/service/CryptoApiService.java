package com.bitcoin.price.chart.service;

import com.bitcoin.price.chart.config.ApiConfig;
import com.bitcoin.price.chart.responseobject.ResponseObject;
import com.bitcoin.price.chart.store.PriceDataEntity;
import com.bitcoin.price.chart.store.PriceDataEntityRepository;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.slf4j.Logger;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Class to get Data from API
 *
 **/
@ApplicationScope
@Component
public class CryptoApiService {
    private final PriceDataEntityMapper mapper;
    private final PriceDataEntityRepository repo;
    private final ApiConfig config;
    private final Logger logger;

    public CryptoApiService(PriceDataEntityMapper mapper, PriceDataEntityRepository repo, ApiConfig config, Logger logger) {
        this.mapper = mapper;
        this.repo = repo;
        this.config = config;
        this.logger = logger;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void afterStartup() {
        storeDataFromAPI();
    }

    /**
     * method to get Data from API
     * parameter loaded from S3
     *
     */
    public ResponseObject getDataFromAPI() {
        ResponseObject responseObject = null;
        Unirest.setTimeouts(0, 0);
        try {
            String url = config.getBaseUrl() +
                    "fsym=" + config.getCrypto() +
                    "&tsym=" + config.getFiat() +
                    "&limit=" + config.getLimit() +
                    "&api_key=" + config.getKey();
            logger.debug("Call to " + config.getBaseUrl());
            Gson gson = new Gson();
            HttpResponse<String> response = Unirest.get(url).asString();

            logger.debug("response status " + response.getStatus());
            logger.debug("response body " + response.getBody());
            responseObject = gson.fromJson(response.getBody(), ResponseObject.class);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return responseObject;
    }

    @Scheduled(initialDelay = 60000, fixedRate = 60000)
    public void storeDataFromAPI() {
        logger.debug("Requesting crypto data...");
        ResponseObject obj = getDataFromAPI();
        List<PriceDataEntity> priceDataEntities = obj.getData().getData().stream()
                .map(mapper::fromPriceData)
                .peek(it -> {
                    it.setCrypto(config.getCrypto());
                    it.setFiat(config.getFiat());
                    it.setServerTimestamp(Timestamp.from(Instant.now()));
                })
                .collect(Collectors.toList());
        repo.saveAll(priceDataEntities);
    }
}

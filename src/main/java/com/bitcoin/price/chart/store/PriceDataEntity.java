package com.bitcoin.price.chart.store;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity(name = "priceData")
@Table(name = "data")
public class PriceDataEntity {
    @Id
    @Column(unique = true)
    @NotNull
    private Long time;
    @NotNull
    private Timestamp serverTimestamp;
    @Column(nullable = false)
    private double high;
    @Column(nullable = false)
    private double low;
    @Column(nullable = false)
    private double open;
    @Column(nullable = false)
    private double volumeFrom;
    @Column(nullable = false)
    private double volumeTo;
    @Column(nullable = false)
    private double close;
    @NotNull
    private String crypto;
    @NotNull
    private String fiat;

    public void setFiat(String conversionSymbol) {
        this.fiat = conversionSymbol;
    }

    public String getFiat() {
        return fiat;
    }

    public void setCrypto(String conversionType) {
        this.crypto = conversionType;
    }

    public String getCrypto() {
        return crypto;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getClose() {
        return close;
    }

    public void setVolumeTo(double volumeTo) {
        this.volumeTo = volumeTo;
    }

    public double getVolumeTo() {
        return volumeTo;
    }

    public void setVolumeFrom(double volumeFrom) {
        this.volumeFrom = volumeFrom;
    }

    public double getVolumeFrom() {
        return volumeFrom;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getOpen() {
        return open;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getLow() {
        return low;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Timestamp getServerTimestamp() {
        return serverTimestamp;
    }

    public void setServerTimestamp(Timestamp serverTimestamp) {
        this.serverTimestamp = serverTimestamp;
    }
}

package com.bitcoin.price.chart.responseobject;

import java.util.Date;
import java.util.List;


public class Data {
    private Boolean Aggregated;
    private Long TimeFrom;
    private Long TimeTo;
    private List<PriceData> Data;

    public Boolean getAggregated() {
        return Aggregated;
    }

    public void setAggregated(Boolean aggregated) {
        Aggregated = aggregated;
    }

    public Long getTimeFrom() {
        return TimeFrom;
    }

    public void setTimeFrom(Long timeFrom) {
        TimeFrom = timeFrom;
    }

    public Long getTimeTo() {
        return TimeTo;
    }

    public void setTimeTo(Long timeTo) {
        TimeTo = timeTo;
    }

    public List<PriceData> getData() {
        return Data;
    }

    public void setData(List<PriceData> data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "Data [Aggregated=" + Aggregated + ", TimeFrom=" + TimeFrom + ", TimeTo=" + TimeTo + ", Data=" + Data
                + "]";
    }


}

package com.bitcoin.price.chart.responseobject;


/*"Response": "Success",
"Message": "",
"HasWarning": false,
"Type": 100,
"RateLimit": {},
"Data": {
    "Aggregated": false,
    "TimeFrom": 1640217600,
    "TimeTo": 1641081600,
    "Data": [
        {
            "time": 1640217600,
            "high": 51382.88,
            "low": 48062.25,
            "open": 48614.97,
            "volumefrom": 30734.43,
            "volumeto": 1525047125.27,
            "close": 50830.2,
            "conversionType": "direct",
            "conversionSymbol": ""
        },
        {
            "time": 1640304000,
            "high": 51863.56,
            "low": 50449.36,
            "open": 50830.2,
            "volumefrom": 21669.58,
            "volumeto": 1107244995.18,
            "close": 50840.36,
            "conversionType": "direct",
            "conversionSymbol": ""
        },*/
public class ResponseObject {
    private String Response;
    private String Message;
    private Boolean HasWarning;
    private Integer Type;
    private Object RateLimit;
    private Data Data;

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Boolean getHasWarning() {
        return HasWarning;
    }

    public void setHasWarning(Boolean hasWarning) {
        HasWarning = hasWarning;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public Object getRateLimit() {
        return RateLimit;
    }

    public void setRateLimit(Object rateLimit) {
        RateLimit = rateLimit;
    }

    public Data getData() {
        return Data;
    }

    public void setData(Data data) {
        Data = data;
    }

    @Override
    public String toString() {
        return "ResponseObject [Response=" + Response + ", Message=" + Message + ", HasWarning=" + HasWarning
                + ", Type=" + Type + ", RateLimit=" + RateLimit + ", Data=" + Data + "]";
    }
}

package com.bitcoin.price.chart.store;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Stream;

public interface PriceDataEntityRepository extends JpaRepository<PriceDataEntity, Long> {
    @Query("select p from priceData p where upper(p.crypto) = upper(?1) and upper(p.fiat) = upper(?2)")
    List<PriceDataEntity> findByCryptoAndFiat(String crypto, String fiat);

    /* Query doesn't work as expected. */
    // @Query("select p from priceData p where p.timeFrom >= ?1 and p.timeTo <= ?2 and upper(p.crypto) = upper(?3) and upper(p.fiat) = upper(?4)")
    // List<PriceDataEntity> findBySymbolPairAndTimeInterval(@NonNull Long timeFrom, @NonNull Long timeTo, @NonNull String conversionType, @NonNull String conversionSymbol);
}
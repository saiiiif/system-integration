package com.bitcoin.price.chart.responseobject;

public class PriceData {
    private Long time;
    private double high;
    private double low;
    private double open;
    private double volumeFrom;
    private double volumeTo;
    private double close;
    private String conversionType;
    private String conversionSymbol;

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getVolumeFrom() {
        return volumeFrom;
    }

    public void setVolumeFrom(double volumeFrom) {
        this.volumeFrom = volumeFrom;
    }

    public double getVolumeTo() {
        return volumeTo;
    }

    public void setVolumeTo(double volumeTo) {
        this.volumeTo = volumeTo;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public String getConversionType() {
        return conversionType;
    }

    public void setConversionType(String conversionType) {
        this.conversionType = conversionType;
    }

    public String getConversionSymbol() {
        return conversionSymbol;
    }

    public void setConversionSymbol(String conversionSymbol) {
        this.conversionSymbol = conversionSymbol;
    }

    @Override
    public String toString() {
        return "PriceData [time=" + time + ", high=" + high + ", low=" + low + ", open=" + open
                + ", volumefrom=" + volumeFrom + ", volumeto=" + volumeTo + ", close=" + close
                + ", conversionType=" + conversionType + ", conversionSymbol=" + conversionSymbol + "]";
    }


}

package com.bitcoin.price.chart;

import com.bitcoin.price.chart.service.CryptoApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.cache.ElastiCacheAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(
        exclude = {
                ElastiCacheAutoConfiguration.class,
                MailSenderAutoConfiguration.class,
                ContextStackAutoConfiguration.class
        })
@EnableScheduling
public class BitcoinPriceChartApplication {
    @Autowired
    public BitcoinPriceChartApplication(CryptoApiService service) {

    }

    public static void main(String[] args) {
        SpringApplication.run(BitcoinPriceChartApplication.class, args);
    }

}

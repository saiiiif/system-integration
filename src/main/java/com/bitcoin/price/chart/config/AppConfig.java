package com.bitcoin.price.chart.config;

public class AppConfig {
    private Long intervalMs;

    public AppConfig() {}

    public Long getIntervalMs() {
        return intervalMs;
    }

    public void setIntervalMs(Long intervalMs) {
        this.intervalMs = intervalMs;
    }
}

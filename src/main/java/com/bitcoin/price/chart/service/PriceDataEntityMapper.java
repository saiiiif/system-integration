package com.bitcoin.price.chart.service;

import com.bitcoin.price.chart.responseobject.Data;
import com.bitcoin.price.chart.responseobject.PriceData;
import com.bitcoin.price.chart.store.PriceDataEntity;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class PriceDataEntityMapper {
    private final ModelMapper mapper = new ModelMapper();

    public PriceDataEntity fromPriceData(PriceData priceData) {
        return mapper.map(priceData, PriceDataEntity.class);
    }

    public Data toData(PriceDataEntity fromEntity) {
        return mapper.map(fromEntity, Data.class);
    }

    public PriceData toPriceData(PriceDataEntity fromEntity) {
        return mapper.map(fromEntity, PriceData.class);
    }
}

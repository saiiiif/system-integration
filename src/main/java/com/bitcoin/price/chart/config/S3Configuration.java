package com.bitcoin.price.chart.config;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import java.io.IOException;
import java.io.InputStreamReader;

@ApplicationScope
@Component
public class S3Configuration {
    private final AmazonS3Client s3Client;
    private final Gson gson;

    public S3Configuration(AmazonS3Client s3Client, Gson gson) {
        this.s3Client = s3Client;
        this.gson = gson;
    }

    @Bean
    public ApiConfig getApiConfiguration() throws IOException {
        S3ObjectInputStream inputStream = s3Client.getObject("system-integration-config", "api.json")
                .getObjectContent();
        InputStreamReader reader = new InputStreamReader(inputStream);
        return gson.fromJson(reader, ApiConfig.class);
    }

    @Bean
    public AppConfig getAppConfiguration() throws IOException {
        S3ObjectInputStream inputStream = s3Client.getObject("system-integration-config", "config.json")
                .getObjectContent();
        InputStreamReader reader = new InputStreamReader(inputStream);
        return gson.fromJson(reader, AppConfig.class);
    }
}
